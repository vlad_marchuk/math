package test.com.math;

import com.math.Formula;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class FormulaTest {
    Formula cut = new Formula();

    static Arguments[] getExpressionTestArgs(){
        return new Arguments[]{
                Arguments.arguments(5,3.8499021093955546),
                Arguments.arguments(10,4.343979255262756),
        };
    }
    @ParameterizedTest
    @MethodSource("getExpressionTestArgs")
    void getExpressionTest(double x,double expected){
        Assertions.assertEquals(expected, cut.getExpression(x));
    }

}