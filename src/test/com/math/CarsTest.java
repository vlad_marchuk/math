package test.com.math;

import com.math.Cars;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class CarsTest {

    Cars cut = new Cars();

    static Arguments[] getRangeCarsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(56,65,100,3,463),
                Arguments.arguments(120,205,65,1,390),
        };
    }

    @ParameterizedTest
    @MethodSource("getRangeCarsTestArgs")
    void getRangeCarsTest(double speedFirstCar,double speedSecondCar,double distanceBetweenCars, double time,double expected){
        Assertions.assertEquals(expected, cut.getRangeCars(speedFirstCar,speedSecondCar,distanceBetweenCars,time));
    }

}