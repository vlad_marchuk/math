package test.com.math;

import com.math.DoubleTriangle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class DoubleTriangleTest {

    DoubleTriangle cut = new DoubleTriangle();

    static Arguments[] findPointInTheAreaTestArgs(){
        return new Arguments[]{
                Arguments.arguments(0,0,true),
                Arguments.arguments(12,20,false),
                Arguments.arguments(0,-0.25,true),
                Arguments.arguments(-0.2,0.2,true),
        };
    }
    @ParameterizedTest
    @MethodSource("findPointInTheAreaTestArgs")
    void findPointInTheAreaTest(double x, double y,boolean expected){
        Assertions.assertEquals(expected, cut.findPointInTheArea(x,y));
    }

}