package test.com.math;

import com.math.Shooting;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class ShootingTest {
    Shooting cut = new Shooting();

    static Arguments[] calculationBulletRangeInRadiansTestArgs(){
        return new Arguments[]{
                Arguments.arguments(100,1,86.50078106670382),
                Arguments.arguments(125,1.3,238.40545686997686),
        };
    }
    static Arguments[] calculationBulletRangeInDegreesTestArgs(){
        return new Arguments[]{
                Arguments.arguments(100,57.2957795,86.50078104862574),
                Arguments.arguments(15,60,2.043515223941081),
        };
    }
    static Arguments[] convertSpeedTestArgs(){
        return new Arguments[]{
                Arguments.arguments(54,15),
                Arguments.arguments(90,25),
        };
    }

    @ParameterizedTest
    @MethodSource("calculationBulletRangeInRadiansTestArgs")
    void calculationBulletRangeInRadiansTest(double startingSpeed, double flightAngleRadians,double expected){
        Assertions.assertEquals(expected, cut.calculationBulletRangeInRadians(startingSpeed,flightAngleRadians));
    }

    @ParameterizedTest
    @MethodSource("calculationBulletRangeInDegreesTestArgs")
    void calculationBulletRangeInDegreesTest(double startingSpeed, double flightAngleDegrees,double expected){
        Assertions.assertEquals(expected, cut.calculationBulletRangeInDegrees(startingSpeed,flightAngleDegrees));
    }
    @ParameterizedTest
    @MethodSource("convertSpeedTestArgs")
    void convertSpeedTest(double startingSpeed,double expected){
        Assertions.assertEquals(expected, cut.convertSpeed(startingSpeed));
    }
}

