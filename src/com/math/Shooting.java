package com.math;

public class Shooting {

    public static final double G = 9.81d;

    public double calculationBulletRangeInRadians(double startingSpeed, double flightAngleRadians){
        double rangeFlight = ((Math.pow(convertSpeed(startingSpeed),2)) / G) / (2 * (Math.sin(flightAngleRadians)) * (Math.cos(flightAngleRadians)));
        return rangeFlight;
    }

    public double calculationBulletRangeInDegrees(double startingSpeed, double flightAngleDegrees){
        double flightAngleRadians = Math.toRadians(flightAngleDegrees);
        double rangeFlight = ((Math.pow(convertSpeed(startingSpeed),2)) / G) / (2 * (Math.sin(flightAngleRadians)) * (Math.cos(flightAngleRadians)));
        return rangeFlight;
    }

    public double convertSpeed(double startingSpeed){
         double startingSpeedMetrSeconds = ((startingSpeed * 5) / 18);
        return startingSpeedMetrSeconds;
    }
}
