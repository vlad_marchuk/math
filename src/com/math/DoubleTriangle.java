package com.math;

public class DoubleTriangle {

    public boolean findPointInTheArea(double x, double y) {
        double x1, y1, x2, y2, x3, y3, x4, y4;
        x1 = -2;
        y1 = 2;
        x2 = 0;
        y2 = 0;
        x3 = 0;
        y3 = -1;
        x4 = 2;
        y4 = 4;

           double firstLeftPartTriangle  =  (x1 - x) * (y2 - y1) - (x2 - x1) * (y1 - y);
           double secondLeftPartTriangle =   (x2 - x) * (y3 - y2) - (x3 - x2) * (y2 - y);
           double thirdLeftPartTriangle =   (x3 - x) * (y1 - y3) - (x1 - x3) * (y3 - y);

           double firstRightPartTriangle  =  (x4 - x) * (y2 - y4) - (x2 - x4) * (y4 - y);
           double secondRightPartTriangle =  (x2 - x) * (y3 - y2) - (x3 - x2) * (y2 - y);
           double thirdRightPartTriangle =   (x3 - x) * (y4 - y3) - (x1 - x3) * (y3 - y);

          if (x == 0 && y > 0){
              return  false;
          }
          if (x == 0 && y < -1){
              return  false;
          }
           if (firstLeftPartTriangle >= 0 && secondLeftPartTriangle >= 0 && thirdLeftPartTriangle >= 0){
               return  true;
           }
            if (firstLeftPartTriangle <= 0 && secondLeftPartTriangle <= 0 && thirdLeftPartTriangle <= 0){
                return  true;
           }

           if (firstRightPartTriangle >= 0 && secondRightPartTriangle >= 0 && thirdRightPartTriangle >= 0){
                return  true;
        }
         if (firstRightPartTriangle <= 0 && secondRightPartTriangle <= 0 && thirdRightPartTriangle <= 0){
                 return  true;
        }
        else
                 return  false;
    }
}





