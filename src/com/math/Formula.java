package com.math;

public class Formula {

    public double getExpression(double x){

        double z =
                ((6 * Math.log(Math.sqrt(Math.exp(x + 1) + ((2 * Math.exp(x)) * Math.cos(x))))) /
                (Math.log(x - Math.exp(x+1) * Math.sin(x)))) + (Math.abs((Math.cos(x) / Math.exp(Math.sin(x)))));
        return  z;
    }


}
