package com.math;

public class Cars {

    public double getRangeCars(double speedFirstCar,double speedSecondCar,double distanceBetweenCars, double time){
        double distanceFirstCar = speedFirstCar * time;
        double distanceSecondCar = speedSecondCar * time;
        double distance = distanceBetweenCars + distanceFirstCar + distanceSecondCar;
        return distance;
    }
}
