package com.math;

public class Main {

    public static void main(String[] args) {
        Shooting shooting = new Shooting();
        Cars cars = new Cars();
        DoubleTriangle doubleTriangle = new DoubleTriangle();
        Formula formula = new Formula();


        //Стрельба из гаубицы. Дан угол возвышения ствола а и начальная скорость полёта снаряда. v км/ч.
        // Вычислить расстояние полёта снаряда. Реализовать решения для угла в градусах и в радианах.



        // Расчет расстояния полета снаряда снаряда, с задаными параматрами: начальна скорость в км/час и угол выстрела в радианах
        System.out.println("Снаряд пролетит расстяние  " + shooting.calculationBulletRangeInRadians(250, 1) + " метров");

        // Расчет расстояния полета снаряда снаряда, с задаными параматрами: начальна скорость в км/час и угол выстрела в градусах
        System.out.println("Снаряд пролетит расстяние  " + shooting.calculationBulletRangeInDegrees(25, 25) + " метров");


        //Скорость первого автомобиля v1 км/ч, второго — v2 км/ч, расстояние между ними s км.
        // Какое расстояние будет между ними через t ч, если автомобили движутся в разные стороны?

        System.out.println("Расстояние между автомобилями будет равно - " + cars.getRangeCars(25, 60, 50, 2) + " километров");


        //true если точка лежит внутри заштрихованной области, иначе — false.

        System.out.println(doubleTriangle.findPointInTheArea(0, -0.65));

        //Вычислить значение выражения

        System.out.println(formula.getExpression(10));
    }
}








